package kosuhina.tm;

import static org.junit.Assert.assertTrue;

import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;
import kosuhina.tm.service.ProjectService;
import kosuhina.tm.service.TaskService;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
  /**
   * Rigorous Test :-)
   */
  @Test
  public void shouldAnswerWithTrue() {

      final Task task = TaskService.getInstance().findByIndex(0);
      System.out.println(task);
      final Project project = ProjectService.getInstance().findByIndex(0);
      System.out.println(project);
      assertTrue(true);

  }

}
