package kosuhina.tm.constant;

public class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String COMMAND_HISTORY = "command-history";
    public static final String EXIT = "exit";


    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_ADD_TO_USER_BY_INDEX = "project-add-to-user-by-index";
    public static final String PROJECT_LIST_PROFILE = "project-list-profile";
    public static final String PROJECTS_TO_JSON = "projects-to-json";
    public static final String PROJECTS_TO_XML = "projects-to-xml";
    public static final String PROJECTS_FROM_JSON = "projects-from-json";
    public static final String PROJECTS_FROM_XML = "projects-from-xml";
    public static final String PROJECTS_JSON_FILE = "projects.json";
    public static final String PROJECTS_XML_FILE = "projects.xml";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";
    public static final String TASK_ADD_TO_USER_BY_INDEX = "task-add-to-user-by-index";
    public static final String TASK_LIST_PROFILE = "task-list-profile";
    public static final String TASKS_TO_JSON = "tasks-to-json";
    public static final String TASKS_TO_XML = "tasks-to-xml";
    public static final String TASKS_FROM_JSON = "tasks-from-json";
    public static final String TASKS_FROM_XML = "tasks-from-xml";
    public static final String TASKS_JSON_FILE = "tasks.json";
    public static final String TASKS_XML_FILE = "tasks.xml";


    public static final String USER_CREATE = "user-create";
    public static final String USER_LIST = "user-list";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_UPDATE_BY_ID = "user-update-by-id";
    public static final String USER_UPDATE_BY_INDEX = "user-update-by-index";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";
    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";
    public static final String USER_REMOVE_BY_INDEX = "user-remove-by-index";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_VIEW_BY_ID = "user-view-by-id";
    public static final String USER_VIEW_INDEX = "user-view-by-index";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_LOGIN = "user-login";
    public static final String USER_LOGOUT = "user-logout";
    public static final String USER_VIEW_PROFILE = "user-view-profile";
    public static final String USER_UPDATE_PROFILE = "user-update-profile";
    public static final String USER_UPDATE_PASSWORD = "user-update-password";

}
