package kosuhina.tm;


import kosuhina.tm.enumerated.Role;
import kosuhina.tm.exception.ProjectNotFoundException;
import kosuhina.tm.exception.TaskNotFoundException;
import kosuhina.tm.listener.ProjectListener;
import kosuhina.tm.listener.TaskListener;
import kosuhina.tm.listener.UserListener;
import kosuhina.tm.publisher.Publisher;
import kosuhina.tm.publisher.PublisherImpl;
import kosuhina.tm.repository.ProjectRepository;
import kosuhina.tm.repository.TaskRepository;
import kosuhina.tm.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;


/**
 * Основной класс
 */
public class Application {

    public static final Logger logger = LogManager.getLogger(Application.class);


   static {
        UserService.getInstance().create("test", "test", Role.USER);
        UserService.getInstance().create("admin", "admin", Role.ADMIN);
        ProjectRepository.getInstance().create("DEMO3 PROJECT 1");
        ProjectRepository.getInstance().create("DEMO9 PROJECT 2");
        ProjectRepository.getInstance().create("DEMO1 PROJECT 3");
        TaskRepository.getInstance().create("TEST5");
        TaskRepository.getInstance().create("TEST3");
        TaskRepository.getInstance().create("TEST9");
        TaskRepository.getInstance().create("TEST7");

    }

    public static void main(final String[] args) {

        System.out.println("***ДОБРО ПОЖАЛОВАТЬ В МЕНЕДЖЕР ЗАДАЧ!***");
        Publisher publisher = new PublisherImpl();
        UserListener userListener = new UserListener();
        ProjectListener projectListener = new ProjectListener();
        TaskListener taskListener = new TaskListener();
        publisher.addListener(userListener);
        publisher.addListener(projectListener);
        publisher.addListener(taskListener);
        try {
            publisher.start(new Scanner(System.in));
        }catch (ProjectNotFoundException | TaskNotFoundException | IOException exception) {
            logger.error(exception.getMessage());
        }
    }


}

