package kosuhina.tm.repository;

import kosuhina.tm.entity.Project;

import java.io.IOException;
import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> {

    private static ProjectRepository instance = null;

    private ProjectRepository() {

    }

    public static ProjectRepository getInstance() {
        if (instance == null) {
            instance = new ProjectRepository();
        }
        return instance;
    }

    private List<Project> projectsByName = new ArrayList<>();
    private Map<String, List<Project>> mapProject;

    public String getName(Project project) {
        if (project == null) return null;
        return project.getName();
    }

    public Long getUserId(Project project) {
        if (project == null) return null;
        return project.getUserId();
    }


    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        if (mapProject == null) {
            mapProject = new HashMap<>();
        }
        if (mapProject.get(name) == null) {
            projectsByName = new ArrayList<>(); }
        projectsByName.add(project);
        mapProject.put(name, projectsByName);
        items.add(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        if (mapProject == null) {
            mapProject = new HashMap<>();
        }
        if (mapProject.get(name) == null) {
            projectsByName = new ArrayList<>(); }
        projectsByName.add(project);
        mapProject.put(name, projectsByName);
        items.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }


    public Project findByIndex(int index) {
        if (index < 0 || index > items.size() -1) return null;
        return items.get(index);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Project project : items) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public List<Project> findProjectsByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final List<Project> projectsByName = mapProject.get(name);
        if(projectsByName == null){
            return null;
        }
        return projectsByName;
    }

    public Project removeByIndex(final int index) {
        final Project task = findByIndex(index);
        if (task == null) return null;
        items.remove(task);
        return task;
    }

    public Project removeById(final Long id) {
        final Project task = findById(id);
        if (task == null) return null;
        items.remove(task);
        return task;
    }

    public List<Project> removeByName(final String name) {
        final List<Project> projectsByName = findProjectsByName(name);
        if (projectsByName == null) return null;
        mapProject.remove(name);
        Iterator<Project> i = items.iterator();
        while (i.hasNext()) {
            Project project = i.next();
            if (project.getName().equals(name)) i.remove();
        }
        return projectsByName;
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project : items) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

}
