package kosuhina.tm.repository;

import kosuhina.tm.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task>{
    private static TaskRepository instance = null;

    private TaskRepository() {

    }

    public static TaskRepository getInstance() {
        if (instance == null) {
            instance = new TaskRepository();
        }
        return instance;
    }

    private static Logger logger = LogManager.getLogger(TaskRepository.class);

        private List<Task> tasksByName = new ArrayList<>();
    private Map<String, List<Task>> mapTask;

    public String getName(Task task) {
        if (task == null) return null;
        return task.getName();
    }

    public Long getUserId(Task task) {
        if (task == null) return null;
        return task.getUserId();
    }




    public Task create(final String name) {
        logger.trace("create --> {}", name);
        final Task task = new Task();
        task.setName(name);
        logger.info("Добавлена задача с наименованием = " + name );
        if (mapTask == null) {
            mapTask = new HashMap<>();
        }
        if (mapTask.get(name) == null) {
            tasksByName = new ArrayList<>();}
        tasksByName.add(task);
        mapTask.put(name, tasksByName);
        items.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        logger.trace("create --> {} {}", name, description);
        final Task task = new Task();
        task.setName(name);
        logger.info("Добавлена задача с наименованием = " + name );
        task.setDescription(description);
        items.add(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description) {
        logger.trace("update --> {} {}", name, description);
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        logger.info("У задачи (ID = "+ id +") изменены наименование/описание.");
        return task;
    }


    public List<Task> findByProjectId(final Long projectId) {
        logger.trace("findByProjectId --> {}", projectId);
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > items.size() -1) return null;
        return items.get(index);
    }

    public List<Task> findTasksByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final List<Task> tasksByName = mapTask.get(name);
        if(tasksByName == null){
            return null;
        }
        return tasksByName;
    }

    public Task removeByIndex(final int index) {
        logger.trace("removeByIndex --> {}", index);
        final Task task = findByIndex(index);
        if (task == null) return null;
        items.remove(task);
        logger.info("Удалена задача с индексом = "+ index);
        return task;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        items.remove(task);
        logger.info("Удалена задача с ID ="+ id);
        return task;
    }

    public List<Task> removeByName(final String name) {
        final List<Task> tasksByName = findTasksByName(name);
        if (tasksByName == null) return null;
        mapTask.remove(name);
        Iterator<Task> i = items.iterator();
        while (i.hasNext()) {
            Task task = i.next();
            if (task.getName().equals(name)) i.remove();
        }
        logger.info("Удалена задача с наименованием = "+ name);
        return tasksByName;
    }


    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task : items) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task update(final Long id, final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setUserId(id);
        logger.info("Задача с индексом = " + index + "назначена пользователю с ID = " + id);
        return task;
    }


    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : items) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if(!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

}
