package kosuhina.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractRepository<E>{
    public ArrayList<E> items = new ArrayList<>();

    public abstract String getName(E item);

    public abstract Long getUserId(E item);

    public List<E> findAll() {
        return items;
    }

    public void clear() {
        items.clear();
    }

    public List<E> findAllByUserId(final Long userId) {
        final List<E> result = new ArrayList<>();
        for (final E item : findAll()) {
            final Long IdUser = getUserId(item);
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(item);
        }
        return result;
    }

    public int unloadJSON(String fileName) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writeValue(file, items);
        return 0;
    }

    public int unloadXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File(fileName), items);
        return 0;
    }



    public int uploadFromJSON(String fileName, Class<E> clazz) throws IOException {
        final File file = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        clear();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        JavaType type = typeFactory.constructCollectionType(List.class, clazz);
        JavaType typeString = typeFactory.constructType(String.class);
        MapType mapType = typeFactory.constructMapType(HashMap.class, typeString, type);
        items = objectMapper.readValue(file, type);
        return 0;
    }

    public int uploadXML(String fileName, Class<E> clazz) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JavaType type = xmlMapper.getTypeFactory().constructCollectionType(List.class, clazz);
        JavaType typeString = xmlMapper.getTypeFactory().constructType(String.class);
        MapType mapType = xmlMapper.getTypeFactory().constructMapType(HashMap.class, typeString, type);
        clear();
        items = xmlMapper.readValue(new File(fileName), type);
        return  0;
    }

}
