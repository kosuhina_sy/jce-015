package kosuhina.tm.repository;
import kosuhina.tm.entity.User;
import kosuhina.tm.service.SessionService;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance = null;

    private UserRepository() {

    }

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    private List<User> users = new ArrayList<>();

    public User add(final User user) {
        users.add(user);
        return user;
    }

    public List<User> findAll() {
        return users;
    }

    public boolean existsByLogin(final String login) {
        final User user = findByLogin(login);
        return user != null;
    }

    public boolean existsById(final Long id) {
        final User user = findById(id);
        return user != null;
    }

    public User findByIndex(final Integer index) {
        if (index<0 || (index > users.size() -1)) return null;
        return users.get(index);
    }

    public User findById(final Long id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    public User findByLogin(final String login) {
        for (final User user: users){
            if(login.equals(user.getLogin())) return user;
        }
        return null;
    }

    public User findByLoginAndPassword(final String login, final String passwordHash) {
        for (final User user: users){
            if(login.equals(user.getLogin())&&passwordHash.equals(user.getPasswordHash())) return user;
        }
        return null;
    }

    public User findByIdAndPassword(final Long id, final String passwordHash) {
        for (final User user: users) {
            if (id.equals(SessionService.getInstance().getSessionId()) && passwordHash.equals(user.getPasswordHash()))
                return user;
        }
        return null;
    }


    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        remove(user);
        return user;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        remove(user);
        return user;
    }

    public User removeByIndex(final int index) {
        final User user = findByIndex(index);
        if (user == null) return null;
        remove(user);
        return user;
    }

    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

}
