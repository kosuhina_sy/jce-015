package kosuhina.tm.publisher;

import kosuhina.tm.exception.ProjectNotFoundException;
import kosuhina.tm.exception.TaskNotFoundException;
import kosuhina.tm.listener.Listener;

import java.io.IOException;
import java.util.Scanner;



    public interface Publisher {
        void addListener(Listener listener);
        void deleteListener(Listener listener);
        void notify(String command, Scanner scanner) throws TaskNotFoundException, ProjectNotFoundException, IOException;
        void start(Scanner scanner) throws ProjectNotFoundException, IOException, TaskNotFoundException;
    }
