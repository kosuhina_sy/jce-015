package kosuhina.tm.publisher;

import kosuhina.tm.exception.ProjectNotFoundException;
import kosuhina.tm.exception.TaskNotFoundException;
import kosuhina.tm.listener.Listener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.ErrorManager;

import static kosuhina.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher{
    private  List<Listener> listeners ;

    public PublisherImpl() {
        this.listeners = new ArrayList<>();
    }

    @Override
    public void start(Scanner scanner) throws ProjectNotFoundException, TaskNotFoundException, IOException {
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            //if(!checkCommand(command)) {continue;}
            notify(command,scanner);
            System.out.println();
        }
    }



    @Override
    public void addListener(Listener listener) {
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void notify(String command,Scanner scanner) throws TaskNotFoundException, ProjectNotFoundException, IOException {
        for (Listener listener : listeners){
            listener.update(command,scanner);
        }
    }}
