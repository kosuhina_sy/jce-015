package kosuhina.tm.listener;

import kosuhina.tm.exception.TaskNotFoundException;
import kosuhina.tm.service.TaskService;

import java.io.IOException;
import java.util.Scanner;

import static kosuhina.tm.constant.TerminalConst.*;

public class TaskListener implements Listener {

    private final TaskService taskService ;

    public TaskListener() {
        this.taskService = TaskService.getInstance();
    }

    @Override
    public int update(String param, Scanner scanner) throws TaskNotFoundException, IOException {
        switch (param) {
            case TASK_LIST: return taskService.listTask();
            case TASK_CLEAR: return taskService.clearTask();
            case TASK_CREATE: return taskService.createTask();
            case TASK_VIEW: return taskService.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskService.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskService.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskService.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskService.updateTaskByIndex();
            case TASK_ADD_TO_USER_BY_INDEX: return taskService.addTaskToUserByIndex();
            case TASK_LIST_PROFILE: return taskService.listTaskProfile();
            case TASKS_TO_JSON: return taskService.unloadToJSON(TASKS_JSON_FILE);
            case TASKS_TO_XML: return taskService.unloadToXML(TASKS_XML_FILE);
            case TASKS_FROM_JSON: return taskService.uploadFromJSON(TASKS_JSON_FILE);
            case TASKS_FROM_XML: return taskService.uploadFromXML(TASKS_XML_FILE);

            default:
                return -1;
        }
    }

}

