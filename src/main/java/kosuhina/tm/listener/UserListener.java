package kosuhina.tm.listener;

import kosuhina.tm.exception.TaskNotFoundException;
import kosuhina.tm.service.UserService;

import java.util.Scanner;

import static kosuhina.tm.constant.TerminalConst.*;

public class UserListener implements Listener {
    private final UserService userService;

    public UserListener() {
        this.userService = UserService.getInstance();
    }


    @Override
    public int update(String param, Scanner scanner) throws TaskNotFoundException {
        switch (param) {
            case USER_CREATE:
                return userService.createUser();
            case USER_LIST:
                return userService.listUser();
            case USER_CLEAR:
                return userService.clearUser();
            case USER_UPDATE_BY_ID:
                return userService.updateUserById();
            case USER_UPDATE_BY_INDEX:
                return userService.updateUserByIndex();
            case USER_UPDATE_BY_LOGIN:
                return userService.updateUserByLogin();
            case USER_REMOVE_BY_INDEX:
                return userService.removeUserByIndex();
            case USER_REMOVE_BY_ID:
                return userService.removeUserById();
            case USER_REMOVE_BY_LOGIN:
                return userService.removeUserByLogin();
            case USER_VIEW_BY_ID:
                return userService.viewUserById();
            case USER_VIEW_INDEX:
                return userService.viewUserByIndex();
            case USER_VIEW_BY_LOGIN:
                return userService.viewUserByLogin();
            case USER_LOGIN:
                return userService.loginUser();
            case USER_LOGOUT:
                return userService.logoutUser();
            case USER_VIEW_PROFILE:
                return userService.viewUserProfile();
            case USER_UPDATE_PROFILE:
                return userService.updateUserProfile();
            case USER_UPDATE_PASSWORD:
                return userService.updateProfilePassword();

            default:
                return -1;

        }
    }
}