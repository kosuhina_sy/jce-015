package kosuhina.tm.listener;

import kosuhina.tm.exception.ProjectNotFoundException;
import kosuhina.tm.service.ProjectService;

import java.io.IOException;
import java.util.Scanner;

import static kosuhina.tm.constant.TerminalConst.*;

public class ProjectListener implements Listener {

    private final ProjectService projectService;

    public ProjectListener() {
        this.projectService = ProjectService.getInstance();
    }


    @Override
    public int update(String param, Scanner scanner) throws ProjectNotFoundException, IOException {
        switch (param) {
            case PROJECT_LIST: return projectService.listProject();
            case PROJECT_CLEAR: return projectService.clearProject();
            case PROJECT_CREATE: return projectService.createProject();
            case PROJECT_VIEW: return projectService.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectService.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectService.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectService.removeProjectByIndex();
            case PROJECT_ADD_TO_USER_BY_INDEX: return projectService.addProjectToUserByIndex();
            case PROJECT_LIST_PROFILE: return projectService.listPojectProfile();
            case PROJECTS_TO_JSON: return projectService.unloadJSON(PROJECTS_JSON_FILE);
            case PROJECTS_TO_XML: return projectService.unloadXML(PROJECTS_XML_FILE);
            case PROJECTS_FROM_JSON: return projectService.uploadFromJSON(PROJECTS_JSON_FILE);
            case PROJECTS_FROM_XML: return projectService.uploadFromXML(PROJECTS_XML_FILE);

            default:
                return -1;
        }
    }
}