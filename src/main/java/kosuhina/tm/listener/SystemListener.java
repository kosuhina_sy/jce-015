package kosuhina.tm.listener;

import kosuhina.tm.service.SystemService;


import java.util.Scanner;

import static kosuhina.tm.constant.TerminalConst.*;

public class SystemListener implements Listener {

    private final SystemService systemService;

    public SystemListener() {
        this.systemService = SystemService.getInstance();
    }

    @Override
    public int update(String param, Scanner scanner)  {
        switch (param) {
            case VERSION: return systemService.displayVersion();
            case ABOUT: return systemService.displayAbout();
            case HELP: return systemService.displayHelp();
            case EXIT: return systemService.displayExit();
            case COMMAND_HISTORY :return systemService.displayCommandHistory();
            default:
                return -1;

        }
    }

}
