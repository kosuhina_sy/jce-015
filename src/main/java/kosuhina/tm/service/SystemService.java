package kosuhina.tm.service;

public class SystemService extends AbstractService{

    public static SystemService instance = null;

    private SystemService() {

    }

    public static SystemService getInstance() {
        if (instance == null) {
            instance = new SystemService();
        }
        return instance;
    }


    public int displayHelp() {
        System.out.println("version - Вывод на экран версии приложения.");
        System.out.println("about - Вывод на экран сведений о разработчике.");
        System.out.println("help - Вывод на экран списка терминальных команд.");
        System.out.println("command-history - Вывод на экран истории введенных терминальных команд.");
        System.out.println("exit - Выход из приложения.");
        System.out.println();
        System.out.println("project-list - Вывод на экран списка проектов.");
        System.out.println("project-create - Создание нового проекта по имени.");
        System.out.println("project-clear - Удаление всех проектов.");
        System.out.println("project-view -  Просмотр проекта по индексу.");
        System.out.println("project-remove-by-name - Удаление проекта по имени.");
        System.out.println("project-remove-by-id - Удаление проекта по id.");
        System.out.println("project-remove-by-index - Удаление проекта по индексу.");
        System.out.println("project-update-by-index - Изменение проекта по индексу.");
        System.out.println("project-add-to-user-by-index - Добавление проекта текущему пользователю по индексу проекта.");
        System.out.println("project-list-profile - Просмотр проектов текущего пользователя.");
        System.out.println("projects-to-xml - Выгрузить данные о проектах в файл формата xml.");
        System.out.println("projects-to-json - Выгрузить данные о проектах в файл формата json.");
        System.out.println("projects-from-xml - Загрузить данные о проектах из файла формата xml.");
        System.out.println("projects-from-json - Загрузить данные о проектах из файла формата json.");



        System.out.println();
        System.out.println("task-list - Вывод на экран списка задач.");
        System.out.println("task-create - Создание новой задачи по имени.");
        System.out.println("task-clear - Удаление всех задач.");
        System.out.println("task-view -  Просмотр задачи по индексу.");
        System.out.println("task-remove-by-name - Удаление задачи по имени.");
        System.out.println("task-remove-by-id - Удаление задачи по id.");
        System.out.println("task-remove-by-index - Удаление задачи по индексу.");
        System.out.println("task-update-by-index - Изменение задачи по индексу.");
        System.out.println("task-list-by-project-id - Вывод на экран списка задач по id проекта.");
        System.out.println("task-add-to-project-by-ids - Добавление задачи по id проектов.");
        System.out.println("task-remove-from-project-by-ids - Удаление задачи по id проектов.");
        System.out.println("task-add-to-user-by-index - Добавление задачи текущему пользователю по индексу задачи.");
        System.out.println("task-list-profile - Просмотр задач текущего пользователя.");
        System.out.println("tasks-to-json - Выгрузить данные о задачах в файл формата json.");
        System.out.println("tasks-to-xml - Выгрузить данные о проектах в файл формата xml.");
        System.out.println("tasks-from-json - Загрузить данные о задачах из файла формата json.");
        System.out.println("tasks-from-xml - Загрузить данные о проектах из файла формата xml.");

        System.out.println();
        System.out.println("user-create - Создание нового пользователя.");
        System.out.println("user-list - Вывод на экран списка пользователей.");
        System.out.println("user-clear - Удаление всех пользователей");
        System.out.println("user-update-by-id - Изменение пользователя по id.");
        System.out.println("user-update-by-index -  Изменение пользователя по индексу.");
        System.out.println("user-update-by-login - Изменение пользователя по логину.");
        System.out.println("user-remove-by-id - Удаление пользователя по id.");
        System.out.println("user-remove-by-index - Удаление пользователя по индексу.");
        System.out.println("user-remove-by-login - Удаление пользователя по логину.");
        System.out.println("user-view-by-id - Просмотр пользователя по id.");
        System.out.println("user-view-by-index - Просмотр пользователя по индексу.");
        System.out.println("user-view-by-login - Просмотр пользователя по логину.");
        System.out.println("user-login - Аутентификация пользователя.");
        System.out.println("user-logout - Выход пользователя.");
        System.out.println("user-view-profile - Проcмотр профиля текущего пользователя.");
        System.out.println("user-update-profile - Изменение профиля текущего пользователя.");
        System.out.println("user-update-password - Изменение пароля текущего пользователя.");

        System.out.println();
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Данные о разработчике
     */
    public int displayAbout() {
        System.out.println("Svetlana Kosuhina");
        System.out.println("lana__svet@list.ru");
        return 0;
    }

    /**
     *Выход из программы
     */
    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    /**
     *Сообщение об ошибке
     */
    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    /**
     *История команд
     */
    public int displayCommandHistory() {
        System.out.println(CommandServise.getHistory());
        return 0;
    }

}
