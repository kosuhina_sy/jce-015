package kosuhina.tm.service;

public class SessionService {
    private static SessionService instance;
    private Long sessionId;

    public static SessionService getInstance() {
        if (instance == null) {
            instance = new SessionService();
            instance.sessionId = null;
        }
        return instance;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

}
