package kosuhina.tm.service;

import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;
import kosuhina.tm.repository.ProjectRepository;
import kosuhina.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    public static ProjectTaskService instance = null;

    private final ProjectService projectService;

    private final TaskService taskService;

    private final TaskRepository taskRepository = TaskRepository.getInstance();

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();


    private ProjectTaskService() {
        projectService = ProjectService.getInstance();
        taskService = TaskService.getInstance();
    }

    public static ProjectTaskService getInstance() {
        if (instance == null) {
            instance = new ProjectTaskService();
        }
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findByProjectId(projectId);
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId,taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

}
