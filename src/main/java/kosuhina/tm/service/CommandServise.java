package kosuhina.tm.service;

import java.util.ArrayList;

public class CommandServise <E> extends ArrayList<E> {

    private static CommandServise history;
        private int maxSize;

        public static CommandServise getHistory() {
            if (history == null) {
                history = new CommandServise();
                history.maxSize = 10;
            }
            return history;
        }


        public CommandServise(int size) {
            super();
            this.maxSize = size;
        }

        public CommandServise() {
            super();
            this.maxSize = 10;
        }

        @Override
        public boolean add(E e) {
            if (this.size() > maxSize - 1) {
                this.remove(0);
            }
            return super.add(e);
        }

        public int getMaxSize() {
            return maxSize;
        }
    }


