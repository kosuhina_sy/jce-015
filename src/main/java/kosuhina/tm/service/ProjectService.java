package kosuhina.tm.service;

import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;
import kosuhina.tm.exception.ProjectNotFoundException;
import kosuhina.tm.repository.ProjectRepository;
import kosuhina.tm.repository.TaskRepository;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Сервисный класс для работы с репозиторием проектов
 */

public class ProjectService extends AbstractService{

    public static ProjectService instance = null;

    private final ProjectRepository projectRepository;

    private ProjectService() { projectRepository = ProjectRepository.getInstance(); }

    public static ProjectService getInstance() {
        if (instance == null) {
            instance = new ProjectService();
        }
        return instance;
    }


    public int createProject(){
        System.out.println("[СОЗДАНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final String name = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОПИСАНИЕ ПРОЕКТА:");
        final String description = scanner.nextLine();
        create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName(){
        System.out.println("[УДАЛЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final String name = scanner.nextLine();
        final List<Project> projects  = removeByName(name);
        if (projects == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectById(){
        System.out.println("[УДАЛЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПРОЕКТА:");
        final long id = scanner.nextLong();
        final Project project  = removeById(id);
        if (project == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex(){
        System.out.println("[УДАЛЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПРОЕКТА :");
        final int index = scanner.nextInt() - 1;
        final Project project  = removeByIndex(index);
        if (project == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException {
        System.out.println("[ИЗМЕНЕНИЕ ПРОЕКТА]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПРОЕКТА:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project  = findByIndex(index);
        if (project == null) {
            throw new ProjectNotFoundException("ПРОЕКТ С ТАКИМ ИНДЕКСОМ НЕ НАЙДЕН");
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final String name = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОПИСАНИЕ ПРОЕКТА:");
        final String description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int clearProject(){
        System.out.println("[УДАЛЕНИЕ ВСЕХ ПРОЕКТОВ]");
        clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listProject(){
        System.out.println("[СПИСОК ПРОЕКТОВ]");
        int index = 1;
        viewProjects(findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[ДАННЫЕ ПРОЕКТА]");
        System.out.println("ID: " + project.getId());
        System.out.println("ИМЯ: " + project.getName());
        System.out.println("ОПИСАНИЕ: " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПРОЕКТА:");
        final int index = scanner.nextInt() - 1;
        final Project project = findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int addProjectToUserByIndex() {
        System.out.println("ДОБАВЛЕНИЕ ПРОЕКТА ТЕКУЩЕМУ ПОЛЬЗОВАТЕЛЮ");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПРОЕКТА:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = findByIndexd(index);
        final long userId = SessionService.getInstance().getSessionId();
        project.setUserId(userId);
        System.out.println("[OK]");
        return 0;
    }

    public int listPojectProfile(){
        System.out.println("СПИСОК ПРОЕКТОВ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ: ");
        final Long id = SessionService.getInstance().getSessionId();
        final List<Project> projects = findAllByUserId(id);
        viewProjects(projects);
        System.out.println("[OK]");
        return 0;
    }

    public void viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) return;
        Collections.sort(projects);
        int index = 1;
        for (final Project project : findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
    }

    public Project create(String name, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект
     */
    public Project findByIndex(int index) {
        return projectRepository.findByIndex(index);
    }

    /**
     * Удаление проекта по индексу
     *
     * @param index индекс проекта
     * @return удаленный проект
     */

    public Project removeByIndex(int index) {
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return удаленный проект
     */

    public Project removeById(Long id) {
        return projectRepository.removeById(id);
    }

    /**
     * Удаление проекта по имени
     *
     * @param name имя проекта
     * @return удаленный проект
     */

    public List<Project> removeByName(String name) {
        return projectRepository.removeByName(name);
    }

    public Project findByIndexd(int index) {
        return projectRepository.findByIndex(index);
    }

    public List<Project> findAll() {return projectRepository.findAll();}

    public List<Project> findAllByUserId(Long userId) {
        if(userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

    public int unloadJSON(final String fileName) throws IOException {
        System.out.println("СОХРАНЕНИЕ ПРОЕКТОВ В "+fileName);
        return projectRepository.unloadJSON(fileName);
    }

    public int unloadXML(final String fileName) throws IOException {
        System.out.println("СОХРАНЕНИЕ ПРОЕКТОВ В "+fileName);
        return projectRepository.unloadXML(fileName);
    }

    public int uploadFromJSON(final String fileName) throws IOException {
        System.out.println("ЗАГРУЗКА ПРОЕКТОВ ИЗ "+fileName);
        return projectRepository.uploadFromJSON(fileName, Project.class);
    }

    public int uploadFromXML(final String fileName) throws IOException {
        System.out.println("ЗАГРУЗКА ПРОЕКТОВ ИЗ "+fileName);
        return projectRepository.uploadXML(fileName, Project.class);
    }

}
