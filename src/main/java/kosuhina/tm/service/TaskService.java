package kosuhina.tm.service;

import kosuhina.tm.entity.Task;
import kosuhina.tm.exception.TaskNotFoundException;
import kosuhina.tm.repository.TaskRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractService{
    private static Logger logger = LogManager.getLogger(TaskService.class);

    private final TaskRepository taskRepository;

    private static volatile TaskService instance = null;

    private TaskService() {
        this.taskRepository = TaskRepository.getInstance();
    }

    public static TaskService getInstance() {
        if (instance == null) {
            synchronized (TaskService.class) {
                if (instance == null) {
                    instance = new TaskService();
                }
            }
        }
        return instance;
    }

    public int removeTaskByName(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧИ ПО ИМЕНИ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final String name = scanner.nextLine();
        final List <Task> tasks  = removeByName(name);
        if (tasks == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧИ ПО ID]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ЗАДАЧИ:");
        final long id = scanner.nextLong();
        final Task task  = removeById(id);
        if (task == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧИ ПО ИНДЕКСУ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ЗАДАЧИ:");
        final int id = scanner.nextInt() - 1;
        final Task task  = removeByIndex(id);
        if (task == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        System.out.println("[ИЗМЕНЕНИЕ ЗАДАЧИ]");
        System.out.println("ПОЖАЛУЙСТА, ВНЕСИТЕ ИНДЕКС ЗАДАЧИ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task  = findByIndex(index);
        if (task == null) {
            logger.error("Задача с индексом = " + String.valueOf(index + 1) + " не найдена");
            throw new TaskNotFoundException("ЗАДАЧА С ТАКИМ ИНДЕКСОМ НЕ НАЙДЕНА");
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final String name = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОПИСАНИЕ ЗАДАЧИ:");
        final String description = scanner.nextLine();
        update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int createTask(){
        System.out.println("[СОЗДАНИЕ ЗАДАЧИ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final String name = scanner.nextLine();
        create(name);
        System.out.println("[OK]");
        return 0;
    }

    public int listTaskProfile(){
        System.out.println("СПИСОК ЗАДАЧ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ: ");
        final Long id = SessionService.getInstance().getSessionId();
        final List<Task> tasks = findAllByUserId(id);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask(){
        System.out.println("[УДАЛЕНИЕ ЗАДАЧ]");
        clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[ПРОСМОТР ЗАДАЧИ]");
        System.out.println("ID ПОЛЬЗОВАТЕЛЯ: " + task.getUserId());
        System.out.println("ID: " + task.getId());
        System.out.println("ИМЯ: " + task.getName());
        System.out.println("ОПИСАНИЕ: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ЗАДАЧИ:");
        final int index = scanner.nextInt() - 1;
        final Task task = findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int listTask(){
        System.out.println("[СПИСОК ЗАДАЧ]");
        int index = 1;
        viewTasks(findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks (final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        Collections.sort(tasks);
        int index = 1;
        for (final Task task: findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int addTaskToUserByIndex() {
        final Long id = SessionService.getInstance().getSessionId();
        if(id == null){
            System.out.println("ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН");
            System.out.println("ID: " + id);
            return 0;
        }
        System.out.println("НАЗНАЧЕНИЕ ЗАДАЧИ ТЕКУЩЕМУ ПОЛЬЗОВАТЕЛЮ ПО ИНДЕКСУ ЗАДАЧИ");
        System.out.println("ПОЖАЛУЙСТА, ВНЕСИТЕ ИНДЕКС ЗАДАЧИ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        addTaskToUser(id, index);
        System.out.println("[OK]");
        return 0;
    }

    public Task create(String name) { return taskRepository.create(name);}

    public Task create(String name, String description) {
        return taskRepository.create(name, description);
    }

    public Task update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if(description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() { taskRepository.clear(); }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public Task removeByIndex(int index) {
        return taskRepository.removeByIndex(index);
    }

    public Task removeById(Long id) {
        return taskRepository.removeById(id);
    }

    public List <Task>  removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task addTaskToUser(long id, int index) {return taskRepository.update(id, index);}

        public List<Task> findAllByUserId(Long userId) {
        if(userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public int unloadToJSON(final String fileName) throws IOException {
        System.out.println("СОХРАНЕНИЕ ЗАДАЧ В "+fileName);
        return taskRepository.unloadJSON(fileName);
    }

    public int unloadToXML(final String fileName) throws IOException {
        System.out.println("СОХРАНЕНИЕ ЗАДАЧ В "+fileName);
        return taskRepository.unloadXML(fileName);
    }

    public int uploadFromJSON(final String fileName) throws IOException {
        System.out.println("ЗАГРУЗКА ЗАДАЧ ИЗ "+fileName);
        return taskRepository.uploadFromJSON(fileName, Task.class);
    }

    public int uploadFromXML(final String fileName) throws IOException {
        System.out.println("ЗАГРУЗКА ЗАДАЧ ИЗ "+fileName);
        return taskRepository.uploadXML(fileName, Task.class);
    }

}
