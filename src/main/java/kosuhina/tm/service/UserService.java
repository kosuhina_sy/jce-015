package kosuhina.tm.service;

import kosuhina.tm.entity.User;
import kosuhina.tm.enumerated.Role;
import kosuhina.tm.repository.UserRepository;
import kosuhina.tm.util.HashUtil;

import java.io.IOException;
import java.util.List;

public class UserService extends AbstractService {

    public static UserService instance = null;

    private final UserRepository userRepository;

    private UserService() { userRepository = UserRepository.getInstance(); }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public int loginUser(){
        System.out.println("[ВХОД В ПРИЛОЖЕНИЕ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН:");
        final String login = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ПАРОЛЬ:");
        final String passwordHash = HashUtil.md5(scanner.nextLine());
        final User user  = findByLoginAndPassword(login, passwordHash);
        if (user == null) {
            System.out.println("[НЕВЕРНЫЙ ЛОГИН ИЛИ ПАРОЛЬ]");
            return 0;
        }
        SessionService sessionService = SessionService.getInstance();
        sessionService.setSessionId(user.getId());
        System.out.println("[OK]");
        return 0;
    }


    public int updateProfilePassword(){
        final Long id = SessionService.getInstance().getSessionId();
        if (id == null) {
            System.out.println("[ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН]");
            return 0;
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ТЕКУЩИЙ ПАРОЛЬ:");
        final String passwordHash = HashUtil.md5(scanner.nextLine());
        final User user  = findByIdAndPassword(id, passwordHash);
        if (user == null) {
            System.out.println("[НЕВЕРНЫЙ ПАРОЛЬ]");
            return 0;
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ НОВЫЙ ПАРОЛЬ:");
        final String passwordHashNew = HashUtil.md5(scanner.nextLine());
        updatePassword(id, passwordHash, passwordHashNew);
        System.out.println("[OK]");
        return 0;
    }

    public int logoutUser(){
        SessionService sessionService = SessionService.getInstance();
        sessionService.setSessionId(null);
        System.out.println("[ВАШ СЕАНС ЗАВЕРШЕН]");
        return 0;
    }

    public int createUser(){
        System.out.println("[СОЗДАНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН:");
        final String login = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ПАРОЛЬ:");
        final String password = scanner.nextLine();
        create(login, password);
        System.out.println("[OK]");
        return 0;
    }

    public int listUser(){
        System.out.println("[СПИСОК ПОЛЬЗОВАТЕЛЕЙ]");
        int index = 1;
        viewUsers(findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user: users) {
            System.out.println(index + ". id:" + user.getId() + " Логин: " + user.getLogin());
            index++;
        }
    }

    public int clearUser(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЕЙ]");
        clear();
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserById(){
        System.out.println("[ИЗМЕНЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final Long id = Long.parseLong(scanner.nextLine());
        final User user  = findById(id);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public int updateUserByIndex(){
        System.out.println("[ИЗМЕНЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПОЛЬЗОВАТЕЛЯ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user  = findByIndex(index);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public int updateUserByLogin(){
        System.out.println("[ИЗМЕНЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН ПОЛЬЗОВАТЕЛЯ:");
        final String login = scanner.nextLine();
        final User user  = findByLogin(login);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public int updateUserProfile(){
        System.out.println("[ИЗМЕНЕНИЕ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ]");
        final Long id = SessionService.getInstance().getSessionId();
        if (id == null) {
            System.out.println("[ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН]");
            return 0;
        }
        final User user  = findById(id);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public void updateUserName(User user){
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ФАМИЛИЮ ПОЛЬЗОВАТЕЛЯ:");
        final String firstName = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПОЛЬЗОВАТЕЛЯ:");
        final String lastName = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОТЧЕСТВО ПОЛЬЗОВАТЕЛЯ:");
        final String middleName = scanner.nextLine();
        update(user.getId(), firstName, lastName, middleName);
        System.out.println("[OK]");
    }

    public int removeUserByIndex(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПОЛЬЗОВАТЕЛЯ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user  = removeByIndex(index);
        if (user == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserById(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final long id = scanner.nextLong();
        final User user  = removeById(id);
        if (user == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН ПОЛЬЗОВАТЕЛЯ:");
        final String login = scanner.nextLine();
        final User user  = removeByLogin(login);
        if (user == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[ДАННЫЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ID: " + user.getId());
        System.out.println("ЛОГИН: " + user.getLogin());
        System.out.println("ФАМИЛИЯ: " + user.getFirstName());
        System.out.println("ИМЯ: " + user.getLastName());
        System.out.println("ОТЧЕСТВО: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    public int viewUserByIndex() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПОЛЬЗОВАТЕЛЯ:");
        final int index = scanner.nextInt() - 1;
        final User user = findByIndex(index);
        viewUser(user);
        return 0;
    }

    public int viewUserById() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final long id = scanner.nextLong();
        final User user = findById(id);
        viewUser(user);
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН ПОЛЬЗОВАТЕЛЯ:");
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        viewUser(user);
        return 0;
    }

    public int viewUserProfile() {
        System.out.println("ПРОФИЛЬ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ:");
        final Long id = SessionService.getInstance().getSessionId();
        if (id == null) {
            System.out.println("[ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН]");
            return 0;
        }
        final User user = findById(id);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        viewUser(user);
        return 0;
    }

    public User create(final String login, final String password, final Role role){
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User updatePassword(final Long id, final String passwordHash,final String passwordHashNew) {
        final User user = findByIdAndPassword(id, passwordHash);
        if (user == null) return null;
        user.setPasswordHash(passwordHashNew);
        return user;
    }

    public User update(final Long id, final String firstName, final String lastName, final String middleName) {
        final User user = findById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

     public User findByIndex(final Integer index) {
        if(index == null) return null;
        return userRepository.findByIndex(index);
    }

    /**
     * Создание пользователя по логину и паролю
     */
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final String passwordHash = HashUtil.md5(password);
        if (existsByLogin(login)) return null;  // если уже есть пользователь с таким логином, его не создавать
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        userRepository.add(user);
        return user;
    }

    public User add(final User user) {
        if (user == null) return null;
        return userRepository.add(user);
    }

    public List<User> findAll() { return userRepository.findAll();  }

    public User findById(final Long id) {
        if(id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User findByLoginAndPassword(final String login, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.findByLoginAndPassword(login, passwordHash);
    }

    public User findByIdAndPassword(final Long id, final String passwordHash) {
        if (id == null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.findByIdAndPassword(id, passwordHash);
    }

    public User removeByIndex(final Integer index) {
        if (index == null) return null;
        return userRepository.removeByIndex(index);
    }
    public User removeById(final Long id) {
        if(id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public void clear() { userRepository.clear(); }

    public boolean existsByLogin(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.existsByLogin(login);
    }

}
